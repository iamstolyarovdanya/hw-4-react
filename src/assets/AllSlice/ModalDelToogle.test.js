import ModalDelToogle , {ToogleModalDel} from "./ModalDelToogle";

describe(`test ModalDelToogle`, () => {
    it(`ModalDelToogle testing first`, () => {
        const payload = `Test`
        const action = ToogleModalDel(payload)
        const initialstate =  {
            isModalDelOpen : null
        }
        const ModalDelToogleTest = ModalDelToogle(initialstate  ,action)
        expect(ModalDelToogleTest.isModalDelOpen).toEqual(payload)
    })
})