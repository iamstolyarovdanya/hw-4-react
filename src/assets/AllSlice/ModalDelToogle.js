import {createSlice} from "@reduxjs/toolkit";

const ModalDelToogle = createSlice({
    name: `ModalDelToogle` ,
    initialState:{
        isModalDelOpen: null
    },
    reducers:{
        ToogleModalDel(state , action){
            state.isModalDelOpen = action.payload
        }
    }
})
export const {ToogleModalDel} = ModalDelToogle.actions;
export default ModalDelToogle.reducer
