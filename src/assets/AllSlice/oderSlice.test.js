import oderSlice, {
    addOder,
    deleteFromOder,
    effectOder,
    deleteAllOders,
    incrementCount,
    decrementCount
} from "./oderSlice";
import {addTobeforOder} from "./addToBeforOder";

describe(`oderSlice tests`, () => {
    it(`oderSlice tests first`, () => {
        const payload =
            {
                "id": 11,
                "name": "Sapphire Brooch",
                "price": 179.99,
                "img": "brooch.jpg",
                "color": "Blue",
                "article": "1122"
            }
        const initialstate = {
            oder: []
        }
        const action = addOder(payload)
        const newSlice = oderSlice(initialstate, action)
        const time = new Date().toISOString()
        expect(newSlice.oder).toEqual([{...payload, count: 1}])

    })
    it(`test for delete item`, () => {
        const payload =
            {
                "id": 11,
                "name": "Sapphire Brooch",
                "price": 179.99,
                "img": "brooch.jpg",
                "color": "Blue",
                "article": "1122",
                "count": 1
            }
        const initialstate = {
            oder: [{
                "id": 11,
                "name": "Sapphire Brooch",
                "price": 179.99,
                "img": "brooch.jpg",
                "color": "Blue",
                "article": "1122",
                "count": 1
            }]
        }
        const actionDel = deleteFromOder(payload)
        const DelSlice = oderSlice(initialstate, actionDel)
        expect(DelSlice.oder).toEqual([])
    })
    it(`test for increment item`, () => {
        const payload =
            {
                "id": 11,
                "name": "Sapphire Brooch",
                "price": 179.99,
                "img": "brooch.jpg",
                "color": "Blue",
                "article": "1122",
                "count": 1
            }
        const initialstate = {
            oder: [{
                "id": 11,
                "name": "Sapphire Brooch",
                "price": 179.99,
                "img": "brooch.jpg",
                "color": "Blue",
                "article": "1122",
                "count": 1
            }]
        }
        const actionInc = incrementCount(payload)
        const DelSlice = oderSlice(initialstate, actionInc)
        expect(DelSlice.oder).toEqual([{
            "id": 11,
            "name": "Sapphire Brooch",
            "price": 179.99,
            "img": "brooch.jpg",
            "color": "Blue",
            "article": "1122",
            "count": 2
        }])
    })
    it(`test for decrement item`, () => {
        const payload =
            {
                "id": 11,
                "name": "Sapphire Brooch",
                "price": 179.99,
                "img": "brooch.jpg",
                "color": "Blue",
                "article": "1122",
                "count": 2
            }
        const initialstate = {
            oder: [{
                "id": 11,
                "name": "Sapphire Brooch",
                "price": 179.99,
                "img": "brooch.jpg",
                "color": "Blue",
                "article": "1122",
                "count": 2
            }]
        }
        const actionDec = deleteFromOder(payload)
        const DelSlice = oderSlice(initialstate, actionDec)
        expect(DelSlice.oder).toEqual([{
            "id": 11,
            "name": "Sapphire Brooch",
            "price": 179.99,
            "img": "brooch.jpg",
            "color": "Blue",
            "article": "1122",
            "count": 1
        }])
    })
    it(`test for dell all item`, () => {
        const payload =
            {
                "id": 11,
                "name": "Sapphire Brooch",
                "price": 179.99,
                "img": "brooch.jpg",
                "color": "Blue",
                "article": "1122",
                "count": 2
            }
        const initialstate = {
            oder: [{
                "id": 11,
                "name": "Sapphire Brooch",
                "price": 179.99,
                "img": "brooch.jpg",
                "color": "Blue",
                "article": "1122",
                "count": 2
            }]
        }
        const actionDelAll = deleteAllOders(payload)
        const DelSlice = oderSlice(initialstate, actionDelAll)
        expect(DelSlice.oder).toEqual([])
    })
    it(`test for do Effect for item`, () => {
        const payload =
            {
                "id": 11,
                "name": "Sapphire Brooch",
                "price": 179.99,
                "img": "brooch.jpg",
                "color": "Blue",
                "article": "1122",
                "count": 2
            }
        const initialstate = {
            oder: []
        }
        const actionEffect = effectOder(payload)
        const DelSlice = oderSlice(initialstate, actionEffect)
        expect(DelSlice.oder).toEqual({
            "id": 11,
            "name": "Sapphire Brooch",
            "price": 179.99,
            "img": "brooch.jpg",
            "color": "Blue",
            "article": "1122",
            "count": 2
        })
    })
})
