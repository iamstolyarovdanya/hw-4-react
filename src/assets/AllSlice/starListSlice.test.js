import starSlice,{addToStar,deleteFromStar , effectStar } from "./starListSlice"

describe(`test starSlice`, () => {
    it(`starSlice testing first`, () => {
        const payload = {user: `Danya` , id: 1}
        const action = addToStar(payload)
        const initialstate =  {
            star : []
        }
        const starSlicetest = starSlice(initialstate  ,action)
        expect(starSlicetest.star).toEqual([payload])
    })
    it(`starSlice testing first`, () => {
        const payload = {user: `Danya` , id: 1}
        const action = deleteFromStar(payload)
        const initialstate =  {
            star : [ {user: `Danya` , id: 1}]
        }
        const starSlicetest = starSlice(initialstate  ,action)
        expect(starSlicetest.star).toEqual([])
    })
    it(`starSlice testing first`, () => {
        const payload = {user: `Danya` , id: 1}
        const action = effectStar(payload)
        const initialstate =  {
            star : []
        }
        const starSlicetest = starSlice(initialstate  ,action)
        expect(starSlicetest.star).toEqual({user: `Danya` , id: 1})
    })
})