import odersDelete , {odersDeleteFun} from "./odersDelete";

describe(`test odersDelete`, () => {
    it(`odersDelete testing first`, () => {
        const payload = `Test`
        const action = odersDeleteFun(payload)
        const initialstate =  {
            deleteOder : []
        }
        const odersDeleteTest = odersDelete(initialstate  ,action)
        expect(odersDeleteTest.deleteOder).toEqual(payload)
    })
})