import FromSlice,{toogleMessageForm} from "./FromSlice";

describe(`test FromSlice`, () => {
    it(`FromSlice testing first`, () => {
        const action = toogleMessageForm()
        const initialstate =  {
            messageForm : false
        }
        const FromSliceTest = FromSlice(initialstate  ,action)
        expect(FromSliceTest.messageForm).toEqual(true)
    })
})