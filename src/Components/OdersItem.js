import React, {useState, useEffect} from 'react'
import {MdDeleteOutline} from "react-icons/md";
import {FaRegStar} from "react-icons/fa";
import {odersDeleteFun} from '../assets/AllSlice/odersDelete';
import {deleteFromStar, addToStar} from '../assets/AllSlice/starListSlice';
import {incrementCount, decrementCount} from "../assets/AllSlice/oderSlice";
import {useDispatch, useSelector} from 'react-redux';
import {ToogleModalDel} from "../assets/AllSlice/ModalDelToogle";
import PropTypes from "prop-types";
import Modal from "./Modal";

const OdersItem = ({item}) => {


    const [addStar, setaddStar] = useState(false)
    const dispatch = useDispatch()
    const deleteModal = useSelector(state => state.isOpenDel.isModalDelOpen)
    useEffect(() => {
        setTimeout(() => {
            const starData = JSON.parse(localStorage.getItem('Selectorstar'));
            if (starData) {
                starData.forEach((element) => {
                    if (element.id === item.id) {
                        change();
                    }
                });
            }
        }, 0.1)
    }, []);

    const change = () => {
        setaddStar(!addStar)

    }
    useEffect(() => {
        const starData = JSON.parse(localStorage.getItem('Selectorstar'));
        if (starData) {
            starData.forEach((element) => {
                if (element.idTime === item.idTime) {
                    change();
                }
            });
        }
    }, []);


    return (
        <div>

            {


                <div className="buy-item">
                    <FaRegStar className={`star_shop ${(addStar) && `star-active`}`} onClick={() => {
                        change();
                        !addStar ? dispatch(addToStar(item)) : dispatch(deleteFromStar(item))
                    }}/>
                    <img src={"images/" + item.img} alt="oders"/>
                    <h2>{item.name}</h2>
                    <p>Color: {item.color}</p>
                    <b> Quantity : {item.count} </b>
                    <button onClick={() => {
                        if (item.count === 1) {
                            dispatch(odersDeleteFun(item))
                            dispatch(ToogleModalDel(item.id))
                        }
                        dispatch(decrementCount(item))
                    }
                    }>-</button>
                    <button onClick={() => dispatch(incrementCount(item))}>+</button>
                    <b>Price : {item.count * Math.round(item.price)} $</b>
                    <MdDeleteOutline className='delete' onClick={() => {
                        dispatch(odersDeleteFun(item))
                        dispatch(ToogleModalDel(item.id))
                    }}/>
                </div>

            }
            {deleteModal === item.id && <Modal
                Modal_text="Do you want delete this item from buy list?"
                titleName="Delete"
                isModalFor={"Delete"}

            />}
        </div>
    )
}
OdersItem.propTypes = {
    item: PropTypes.object.isRequired,
}
export default OdersItem