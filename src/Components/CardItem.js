import React, { useState, useEffect } from 'react';
import Modal from "./Modal";
import { FaRegStar } from "react-icons/fa";
import PropTypes from 'prop-types';
import Button from './Button'
import { addToStar, deleteFromStar } from '../assets/AllSlice/starListSlice';
import { useDispatch, useSelector } from 'react-redux';
import { addTobeforOder } from "../assets/AllSlice/addToBeforOder";
import { modalToogleApprove } from '../assets/AllSlice/TogleAproveModal';
import {deleteFromStarID} from '../assets/AllSlice/starListSlice'
function CardItem({ itemCard,
   }) {
  const [addStar, setaddStar] = useState(false)
  const [stanBtn, setStanBtn] = useState(false);

  const dispatch = useDispatch()
  const selector = useSelector(state => state.star.star);
  const selectorOder = useSelector(state => state.oder.oder);
  const isExist = selectorOder.some(r => r.id === itemCard.id)
  const selectorModalApprove = useSelector(state => state.isOpenApprove.isOpenApprove);
  const change = () => {
    setaddStar(!addStar)
  }

  useEffect(() => {
    const starData = JSON.parse(localStorage.getItem('Selectorstar'));

    if (starData) {
      starData.forEach((element) => {
        if (element.id === itemCard.id) {
          change();

        }
      });
    }
  }, []);

  return (
    <>
      <div className="card_item">

        <FaRegStar className={`star ${(addStar) && `star-active`}`} onClick={() => {
          change();

          !addStar ? dispatch(addToStar(itemCard)) : dispatch(deleteFromStar(itemCard));

        }} />
        <h2 > {itemCard.name}</h2>
        <b>price : {itemCard.price} $</b>
        <img src={"images/" + itemCard.img} width="160" height="155" alt="photoww" />
        <p >color : {itemCard.color}</p>
        {isExist && <p id='already-In-Buy-list'>У кошику</p>}
        <Button Click={() => {
          dispatch(addTobeforOder(itemCard))
           dispatch(modalToogleApprove(itemCard.id))
        }} Text={'Добавити до корзини'} />
      </div >
      {selectorModalApprove === itemCard.id  && <Modal Modal_text="Do you want add this item in buy list?"
                                                       titleName="Approve"
                                                       isModalFor={`Approve`}
      />}

    </>
  );
}
CardItem.propTypes = {
    itemCard : PropTypes.object.isRequired,
    }
export default CardItem






