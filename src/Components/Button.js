import React, {useState} from 'react'

const Button = ({Click , Text}) => {

    return (
        <>
            {
                <button
                    onClick={() => {
                        Click()
                    }}>{Text}</button>
            }


        </>
    )
}

export default Button