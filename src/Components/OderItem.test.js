import {render , screen } from "@testing-library/react";
import OdersItem from "./OdersItem";
import {Provider} from "react-redux";
import store from "../assets/store";
it(`OderItem tets (Snapshot)` , ()=>{
    const item =  {
            "id": 1,
            "name": "Stoper Charm",
            "price": 312,
            "img": "Stopper_Charm.webp",
            "color": "Silver",
            "article": "122",
           "count": 1
        }

   const oder =  render (  <Provider store={store}>
       <OdersItem item={item}/>
   </Provider>)
    expect(oder).toMatchSnapshot()
})