import React, {useState , useContext} from 'react';
import {MdOutlineShoppingCart} from "react-icons/md";
import {FaRegStar} from "react-icons/fa";
import PropTypes from 'prop-types';
import {Link, Outlet,} from 'react-router-dom';
import {useSelector} from 'react-redux';
import Context, {MyCreateContext} from "./Context";

function Header() {

    const {falseTable, trueTable, isTable} = useContext(MyCreateContext)
    const selectorStar = useSelector(state => state.star.star)
    const selectOder = useSelector(state => state.oder.oder);
const formMessage = useSelector(state => state.FormSlice.messageForm)
console.log(`isTable`, isTable)
    return (
        <>
            <div className="header">
                <span className="logo"><Link to="/">Jewelry Paradise</Link></span>
                <ul>
                    <p className='oder_counter'>{selectOder.length}</p>

                    <Link to="/Shop"> <MdOutlineShoppingCart className={` icon-buy `}/></Link>

                    <p className='star_counter'> {selectorStar.length}</p>
                    <Link to="/Star"> <FaRegStar className={` icon-buy `}/></Link>

                    <li>About as</li>
                    <li>Contacts</li>
                    <li className={`home-page`}><Link to="/"> Home page</Link></li>
                </ul>

            </div>
            <main   style={{position: "relative"}}>

                {formMessage && <div className={'message-after-form'} > Покупка була успішно зроблена. Дякуємо за покупку!! </div> }
                <Outlet/>
            </main>
        </>
    );
}

export {Header};