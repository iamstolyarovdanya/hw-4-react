import {screen, render, fireEvent} from "@testing-library/react";
import Modal from './Modal'
import store from "../assets/store";
import {Provider} from "react-redux"
import '@testing-library/jest-dom/extend-expect';

it(`Modal test`, () => {
    const Modal_text = "Do you want add this item in buy list?"
    const titleName = `Approve`

    render(<Provider store={store}>
        <Modal Modal_text={Modal_text} titleName={titleName}/>
    </Provider>)
    // render(<Modal Modal_text={Modal_text} titleName={titleName}/>,{ wrapper: ReduxWrapper })
    const title = screen.getByText(`Approve`)
    const tetxModal = screen.getByText("Do you want add this item in buy list?")
    expect(title).toBeInTheDocument()
    expect(tetxModal).toBeInTheDocument()
})