import React, {useState} from 'react';
import {useFormik} from "formik";
import * as Yup from 'yup'
import {deleteAllOders} from "../assets/AllSlice/oderSlice";
import {useDispatch} from "react-redux";
import {useNavigate} from "react-router-dom";
import {useSelector} from "react-redux";
import {toogleMessageForm} from '../assets/AllSlice/FromSlice'

const Form = () => {

    const [Load, setLoad] = useState(false)
    const selectOder = useSelector(state => state.oder.oder)
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const valid = Yup.object().shape({
        name: Yup.string().required(`This place is required`).matches(/^[\p{L}]+$/u, 'Only letters are allowed'),
        surname: Yup.string().required(`This place is required`).matches(/^[\p{L}]+$/u, 'Only letters are allowed'),
        age: Yup.number().typeError('Only numbers are allowed').required(`This place is required`).positive(`Age can't be negative`),
        address: Yup.string().required(`This place is required`),
        mobile: Yup.string().matches(/^[0-9]{10}$/, "Телефон  форматі (XXX) XXX-XXXX"). typeError('Only numbers are allowed').required('This place is required')

    })

    const formik = useFormik(
        {
            initialValues: {
                name: '',
                surname: '',
                age: '',
                address: '',
                mobile: '',
            },

            onSubmit: values => {

                setLoad(true)
                setTimeout(() => {
                    setLoad(false)
                    console.log(`values`, values)
                    console.log(`selectOder`, selectOder)
                    dispatch(deleteAllOders())
                    formik.resetForm()
                    navigate("/");

dispatch(toogleMessageForm())
                    setTimeout(()=>{

                        dispatch(toogleMessageForm())
                    },7000)
                }, 1000)


            },
            validationSchema: valid,
        }
    )

    return (
        <div className='warpper-form'>
            <form onSubmit={formik.handleSubmit}>
                <h2>Please write your private information</h2>
                <div className={'wrapper-input'}>
                    <label htmlFor="name">
                        <p className="form-label-text">Name :</p>
                        <input
                            name={'name'}
                            id="name"
                            placeholder={`Enter please your name`}
                            onChange={formik.handleChange}
                            value={formik.values.name}
                        />
                    </label>
                    {formik.errors.name && formik.touched.name && <p className="text-error">{formik.errors.name}</p>}
                </div>
                <div  className={'wrapper-input'}>
                    <label htmlFor={'surname'}>
                        <p className="form-label-text">Surname:</p>
                        <input
                            name={'surname'}
                            id={'surname'}
                            placeholder={`Enter please your surname`}
                            value={formik.values.surname}
                            onChange={formik.handleChange}
                        />
                    </label>
                    {formik.errors.surname && formik.touched.surname &&
                        <p className="text-error">{formik.errors.surname}</p>}
                </div>
                <div  className={'wrapper-input'}>
                    <label htmlFor={'age'}>
                        <p className="form-label-text">Your Age :</p>
                        <input
                            name={'age'}
                            id={'age'}
                            placeholder={`Enter please your age`}
                            onChange={formik.handleChange}
                            value={formik.values.age}
                        />
                    </label>

                    {formik.errors.age && formik.touched.age && <p className="text-error">{formik.errors.age}</p>}
                </div>
                <div   className={'wrapper-input'}>
                    <label htmlFor={'address'}>
                        <p className="form-label-text">Your address:</p>
                        <input
                            name={'address'}
                            id={'address'}
                            placeholder={`Enter please your address`}
                            onChange={formik.handleChange}
                            value={formik.values.address}
                        />
                    </label>

                    {formik.errors.address && formik.touched.address &&
                        <p className="text-error">{formik.errors.address}</p>}
                </div>
                <div  className={'wrapper-input'}>
                    <label htmlFor={'mobile'}>
                        <p className="form-label-text">Your telephone number:</p>
                        <input
                            className={'input'}
                            name={'mobile'}
                            id={'mobile'}
                            placeholder={`Enter please your mobile`}
                            onChange={formik.handleChange}
                            value={formik.values.mobile}
                        />
                    </label>
                    {formik.errors.mobile && formik.touched.mobile &&
                        <p className="text-error">{formik.errors.mobile}</p>}
                </div>
                <button type='submit'>buy now</button>
            </form>
            {Load && <div className="blur ">
                <div className="loader "></div>
            </div>}
        </div>
    );
};

export {Form};