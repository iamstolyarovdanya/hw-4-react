import React, {useContext, useState} from 'react';

export const MyCreateContext = React.createContext({isTable: false});
export const Context = ({children}) => {
    const [isTable , setIsTable] =  useState(false)
    const falseTable = ()=>{
        setIsTable(false)
    }
    const trueTable = ()=>{
        setIsTable(true)
    }
    const  value = {falseTable , trueTable , isTable}
    return <MyCreateContext.Provider value={value}>{children}</MyCreateContext.Provider>;
};


// export default Context;

// import React, { createContext, useContext } from "react";
//
// export const ThemeContext = createContext({
//     isTable: false
// });
//
// export const Context = ({ children }) => {
//     const contextValue = {
//         isTable: false, // начальное значение
//         toggleTable: () => {} // заглушка для toggleTable
//     };
//
//     return (
//         <ThemeContext.Provider value={contextValue}>
//             {children}
//         </ThemeContext.Provider>
//     );
// };
//
// export const useTheme = () => {
//     return useContext(Context);
// };