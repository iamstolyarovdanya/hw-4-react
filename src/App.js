import './App.css';
import React, {useContext, useEffect, useState} from "react";
import AppRoutes from './Components/AppRoutes';
import {useSelector, useDispatch} from 'react-redux';

import {effectStar} from './assets/AllSlice/starListSlice';
import {effectOder} from './assets/AllSlice/oderSlice';
import {fetchTodos} from './assets/AllSlice/itemSlice';

import {Context} from "./Components/Context";


function App() {

    const dispatch = useDispatch()
    const selectorStar = useSelector(state => state.star.star)
    const selectorOders = useSelector(state => state.oder.oder)
    const selectorItem = useSelector(state => state.item.item)

    useEffect(() => {
        dispatch(effectOder(JSON.parse(localStorage.getItem(`selectorOders`)) || []))
    }, [])

    useEffect(() => {
        localStorage.setItem(`selectorOders`, JSON.stringify(selectorOders))
    }, [selectorOders])


    useEffect(() => {
        dispatch(effectStar(JSON.parse(localStorage.getItem(`Selectorstar`)) || []))
    }, [])

    useEffect(() => {
        localStorage.setItem(`Selectorstar`, JSON.stringify(selectorStar))
    }, [selectorStar])
    useEffect(() => {

        dispatch(fetchTodos());
    }, []);


    return (<>
            {<div className="App">
                {/*<Form style={{marginBottom: '30px'}} />*/}
                {/*<MyCreateContext>          </MyCreateContext>*/}
                <Context>
                    {/*<Hooks/>*/}
                    <AppRoutes/>
                </Context>
                    {/*<Hooks/>*/}






            </div>}
        </>
    );
}


export default App;
