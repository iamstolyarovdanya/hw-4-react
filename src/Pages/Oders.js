import OdersItem from '../Components/OdersItem';
import {useSelector} from 'react-redux';
import {Link} from 'react-router-dom'


function Oders() {
    const selectOder = useSelector(state => state.oder.oder);
    let allPrice = 0;
    selectOder.forEach((el) =>{

allPrice += (Math.round(el.price) * el.count)
    })
    return (
        <>
        <div className='Oders-wrapper'>

            {selectOder.length > 0 ?
                selectOder.map((el) => (
                    <OdersItem
                        key={el.id}
                        item={el}

                    />
                )) : <div className='Nothing'> Did you chose something? No.</div>}


        </div>

            { selectOder.length > 0 &&     <div className='wrapper-btn-buy'> <Link className="link-to-form " to="/Form">Buy now</Link>  <p>All price {allPrice} $</p></div>}


        </>
    );
}


export {Oders};