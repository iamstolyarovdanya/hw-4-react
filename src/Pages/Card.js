import CardItem from "../Components/CardItem";

import {useSelector} from "react-redux";
import React, {useContext} from "react";
import {MyCreateContext} from "../Components/Context";
import Button from "../Components/Button";
import { FaTableList } from "react-icons/fa6";
import { BsTable } from "react-icons/bs";
import Hooks from "../Hooks/hooks";
function Card() {
    const selector = useSelector(state => state.item.item);
    const {falseTable, trueTable, isTable} = useContext(MyCreateContext)

    return (
        <>
            <FaTableList style={{  width: '45px', height: '45px', color: isTable ? `white` : ` rgba(218, 171, 69, 0.51)`  }} onClick={() => {
                trueTable()
            }} ></FaTableList>
            <BsTable  style={{ width: '45px', height: '42px',color: isTable ? ` rgba(218, 171, 69, 0.51)` : `white` }} onClick={() => {
                falseTable()
            }} ></BsTable>
            <div className={ isTable ? `card-row `: `card`} style={{flexDirection: isTable ? `column` : `row`}}>

                {selector.map((el) => (
                    <CardItem key={el.id}
                              itemCard={el}
                    />
                ))
                }

            </div>
        </>
    );
}

export {Card};