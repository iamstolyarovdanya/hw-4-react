import React from 'react';

import { FaRegStar } from "react-icons/fa";
import { useSelector , useDispatch } from 'react-redux';
import { deleteFromStar } from '../assets/AllSlice/starListSlice';
function Star() {
    const stars = useSelector(state => state.star.star)
    const dispatch = useDispatch()
    return (
        <div  className='Oders-wrapper'>
            {
                stars.length > 0 ?
                stars.map((el)=>( 
                    <div className="buy-item" key={el.id}>
                        <FaRegStar className='Star-delete' style={{color: 'orange'}} onClick={()=>{
                      
                          dispatch(deleteFromStar(el))
                        }} />
                        <img src={"images/" + el.img} alt="oders"/>
                        <h2>{el.name}</h2>
                        <p>color: {el.color}</p>
                        <b>{el.price} $</b>
                    </div>
                )) : <div className='Nothing'> Did you add something? No.</div>
            }

        </div>
    );
}

export {Star};